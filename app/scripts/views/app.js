define([
'backbone',
'views/slides',
'collections/slides',
'models/slide',
'router'
],

function(Backbone, SlidesView, SlidesCollection, SlideModel, MainRouter)
{
    var AppView = Backbone.View.extend({
        el: 'body',

        initialize: function() {
            var testCollection = [
                new SlideModel({title: 'My First Slide'}),
                new SlideModel({title: 'My Second Slide'})

            ];

            new SlidesView({
                collection: new SlidesCollection(testCollection)
            });

            App.router = new MainRouter();
            Backbone.history.start();
        },

        events: {
            'keyup': 'keyUp'
        },

        keyUp: function(e) {
            if (e.keyCode === 37 || e.keyCode === 39) {
                App.Vent.trigger('changeSlide', {
                    direction: e.keyCode === 39 ? 'next' : 'prev'
                });
            }
        }
    });

    return AppView;
});