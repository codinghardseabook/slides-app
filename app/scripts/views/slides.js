define(['backbone', 'views/slide'], function(Backbone, SlideView){
    var SlidesView = Backbone.View.extend({
        el: $('.slides'),

        initialize: function() {
            this.currentIndex = 1;
            this.numSlides = this.collection.length;
            this.transitionSpeed = 800;

            this.renderAll();

            App.Vent.on('init', this.hideAllButFirst, this);
            App.Vent.on('changeSlide', this.changeSlide, this);
        },

        hideAllButFirst: function() {
            this.$el.children(':nth-child(n+2)').hide();
        },

        changeSlide: function(opts) {
            var self = this;
            var newSlide;
            var slides = this.$el.children();

            this.setCurrentIndex(opts);
            newSlide = this.fetchNewSlide(slides);
            this.animateToNewSlide(slides, newSlide, opts.direction);

            App.router.navigate('/slides/' + this.currentIndex);
        },

        setCurrentIndex: function(opts) {
            if (opts.slideIndex) {
                return this.currentIndex = ~~opts.slideIndex;
            }

            this.currentIndex += opts.direction === 'next' ? 1 : -1;

            if (this.currentIndex > this.numSlides) {
                // Go back to slide number 1
                this.currentIndex = 1;
            }

            if (this.currentIndex <= 0) {
                this.currentIndex = this.numSlides;
            }
        },

        fetchNewSlide: function(slides) {
            return slides.eq(this.currentIndex - 1);
        },

        animateToNewSlide: function(slides, newSlide, direction) {
            slides.filter(':visible')
                .css('position', 'absolute')
                .animate({
                    top: direction === 'next'? '100%' : '-100%',
                    opacity: 'hide'
                }, this.transitionSpeed, function() {
                    // after transition is done
                    $(this).css('top', '0');
                    newSlide
                        .css('position', 'absolute')
                        .css('top', direction === 'next' ? '-100%' : '100%')
                        .animate({
                            top: 0,
                            opacity: 'show'
                        }, self.transitionSpeed);
                });
        },

        renderAll: function() {
            this.$el.empty();
            this.collection.each(this.render, this);
        },

        render: function(slide) {
            var slideView = new SlideView({model: slide});
            this.$el.append(slideView.render().el);

            return this;
        }
    });

    return SlidesView;
});